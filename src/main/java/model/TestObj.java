package model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DBKTMD0 - Tarek Md on 26/10/2021
 */

@Getter
@Setter
public class TestObj {

    private final String myStr;

    public TestObj(String str){
        this.myStr = str;
    }
}
