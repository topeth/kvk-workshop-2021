import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author DBKTMD0 - Tarek Md on 23/10/2021
 */
public class FunctionalProgramming {
    static List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

    public static void main(String[] args) {
        //higher order functions
        Collections.sort(numbers, new Comparator<Integer>() {
            @Override
            public int compare(Integer n1, Integer n2) {
                return n1.compareTo(n2);
            }
        });

        Collections.sort(numbers, Comparator.naturalOrder());
        //met lambda expression
        Collections.sort(numbers, Integer::compareTo);

        Function<Double, Double> log = Math::log; //base e log = ln
        Function<Double, Double> sqrt = (value) -> Math.sqrt(value); //square root
        Function<Double, Double> logThenSqrt = sqrt.compose(log);
        System.out.println("logThenSqrt: " + logThenSqrt.apply(3.14));
        // Output: 1.06
        Function<Double, Double> sqrtThenLog = sqrt.andThen(log);
        System.out.println("sqrtThenLog: " + sqrtThenLog.apply(3.14));
        // Output: 0.57
    }

    //pure functions
    //1 2 3 4 5 1 2 3 4 5
    Integer sum(List<Integer> numbers) {

//        List<Integer> list = new ArrayList<>();
//
//        for(Integer integer : numbers){
//            numbers.add(integer);
//        }

        return numbers.stream().mapToInt(Integer::intValue).sum();
    }
}
