import enums.Level;
import enums.LevelStr;
import model.TestObj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

/**
 * @author DBKTMD0 - Tarek Md on 26/10/2021
 */
public class Playground {

    Level level = Level.HIGH;
    LevelStr levelStr = LevelStr.LOW;
    public String testString = "testString";

    public static void main(String[] args) throws IOException {

        Resources app = new Resources();

        //String fileName = "database.properties";
        String fileName = "json/file3.json";

        System.out.println("getResourceAsStream : " + fileName);
        InputStream is = null;
        try {
            is = app.getFileFromResourceAsStream(fileName);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.fillInStackTrace().getMessage());

        } catch (NullPointerException ex) {
            printInputStream(is);
            System.out.println(ex.fillInStackTrace().getMessage());
        } finally {
            System.out.println("FINALLY!");
        }


//
//        System.out.println(level.getLevelCode());
//        System.out.println(levelStr.getLevelVal());
//
//        //komt uit andere applicatie
//        String testStringInput = "teststring";
//
//        //primitive style
//        if (doSomething("") != null) {
//
//        } else {
//
//        }
//
//        //Optional style
//        Optional<TestObj> testObj = Optional.of(doSomething(""));
//        testObj.orElse(new TestObj(testString));
//        testObj.orElseThrow(IOException::new);
    }

    TestObj doSomething(String testString) {
        return new TestObj(testString);
    }

    // print input stream
    private static void printInputStream(InputStream is) throws IOException {
        InputStreamReader streamReader =
                new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(streamReader);

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
    }
}
