package enums;

/**
 * @author DBKTMD0 - Tarek Md on 23/10/2021
 */
public enum LevelStr {
    LOW ("Low"),
    MEDIUM ("Medium"),
    HIGH ("High");

    private final String levelVal;
    LevelStr(String val) {
        this.levelVal = val;
    }
    public String getLevelVal(){
        return this.levelVal;
    }
}
