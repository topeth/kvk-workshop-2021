package enums;

/**
 * @author DBKTMD0 - Tarek Md on 23/10/2021
 */
public enum Level {
    LOW (1),
    MEDIUM (2),
    HIGH (3),
    SUPER_LOW(0);

    private final int levelCode;
    Level(int code) {
        this.levelCode = code;
    }

    public int getLevelCode(){
        return this.levelCode;
    }
}
